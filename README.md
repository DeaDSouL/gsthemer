# gsthemer

This is a simple script to manage your Gnome-Shell themes via cli

- - -

###Installing gsthemer:
 
1. Go to the directory that contains the `gsthemer` script.

        $ cd /path/to/gsthemer/

2. Making the `gsthemer` executable.

        $ chmod +x gsthemer

3. Install the `gsthemer`:

        $ sudo ./gsthemer --install

 The `--install` is going to:

 1. Create `themes/` folder in `/usr/share/gnome-shell/`
 * Move `/usr/share/gnome-shell/theme/` to `/usr/share/gnome-shell/themes/Default/`
 * Create a symbolic-link from `/usr/share/gnome-shell/themes/Default/` to `/usr/share/gnome-shell/theme`
 * Copy the script to `/opt/gsthemer` 
 * Create a symbolic-link from `/opt/gsthemer` to `/usr/bin/gsthemer` so that any user can use it, specially sudoers.
 
- - -

###Uninstall gsthemer

1. Just type:

        $ sudo gsthemer --uninstall
        
 Then it will ask for your confirmation. just type `y` then hit `ENTER`.
 
  The `--uninstall` is going to:
  
  1. Remove the symbolic-link `/usr/share/gnome-shell/theme`.
  * Move `/usr/share/gnome-shell/themes/Default/` to `/usr/share/gnome-shell/theme`.
  * Remove `/usr/share/gnome-shell/themes/` .
  * Remove the symbolic-link `/usr/bin/gsthemer`.
  * Keep the `/opt/gsthemer` as it is, in case you want to use it later.

- - -

###How to add new theme:

    $ sudo gsthemer --add THEME.EXT
 Or you can use `-a` instead of `--add`.
 
 While `THEME.EXT` is an archived gnomeshell theme.
 
 Then you'll be asked whether you want to use it or not. If you do, just type `y` then `ENTER`. Finally hit `ALT+F2` then type `r` to restart the gnome-shell and load the new added theme.

- - -

###How to manage themes:

    $ sudo gsthemer --manage
 Or you ca use `-m` instead of `--manage`.
 Then it will show you something like:
 
    --------------------------------------------------------------------------------

    Found 5 theme(s)

      1) Default
    * 2) Elementary-Luna-3.4
      3) Orta
      4) SLAVE
      5) Smooth-Inset-3.4

    --------------------------------------------------------------------------------

    Commands:
        use    To select and use a theme. (ex: use THEME_NUMBER)
        cp     To copy a theme. (ex: cp THEME_NUMBER new_name)
        rm     To remove a theme. (ex: rm THEME_NUMBER)
        q      To quit.

    Examples:
        use 1
        cp 4 new_theme_name
        rm 2

    --------------------------------------------------------------------------------
    $ 
 
 The stared `*` theme means it's the current used one. In this case we are using `Elementary-Luna-3.4`.

####To use `Default`:
 While you're still in the `manage mode` just type:
 
    use 1

####To copy `SLAVE`:
 While you're still in the `manage mode` just type:
 
    cp 4 xyz_new_name

####To remove `Orta`:
 While you're still in the `manage mode` just type:
 
    rm 3

- - -

###Some notes:

 * gsthemer comes with absolutely no waranty.
 * The theme must be archived. And should contain the `theme.json` file.
 * gsthemer supports the following archive types: `(tar, tar.gz, tgz, tar.z, tar.bz2, rar, zip)`.
 * To see the help menu type `gsthemer --help` or `gsthemer -h`.

- - -

###Author:
 Mubarak Alrashidi (DeaDSouL)

- - -

###License:
 GNU General Public License (GPL)

- - -
